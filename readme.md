# Front-end readme

## Požadavky
Kompilace CSS (SCSS) a JS je řešena přes npm skripty.
Nastavení kompilace a soubory potřebné pro kompilaci jsou ve složce _.tools_.

Pro spuštění je třeba mít nainstalované node.js - stáhnout a nainstalovat z [https://nodejs.org/](https://nodejs.org/)
Poté stačí ve složce _.tools_ spustit příkaz `npm install`.

Poznámka: Po `npm install` se ve složce _.tools_ objeví složka _node_modules_. Tuto složku nedávejte do gitu!

Task se spustí příkazem `npm run [názek tasku]`, kde název tasku je definován v souboru `package.json` v sekci `scripts`. 
Tzn. například spojení javascript souborů a minifikace výsledného souboru se provede taskem `npm run js`.

V PHPstormu lze tasky spouštět i přímo z IDE přes grafické rozhraní. Po kliknutí pravým tlačítkem na soubor package.json zvolte položku Show npm Scripts. Otevře se panel s definovanými npm tasky, které lze spustit dvojklikem na název tasku. Výstup z tasku se pak zobrazuje v panelu Run.

K dispozici jsou následující tasky

- `icons` - vytvoří sprite mapu z svg souborů umístěných ve složce images/icon a připraví proměnné pro použití v scss souborech
- `js` - spojení a minifikace js souborů 
- `css:dev` - automaticky přeloží SCSS soubory při jejich změně (běží jako watch task - tj. sleduje změny), neminifikuje výsledné CSS!
- `css:prod` - zminifikuje výsledné CSS a doplní vendor prefixy (nespouští watch task - tj. nesleduje změny)
- `watch:js` - sleduje změny v js souborech a automaticky spouští task `js`
- `watch` - spustí `watch:js` a `css:dev` zároveň, tj. jsou sledované js a scss soubory na změny a automaticky se kompilují po změně
- `prod` - zkompiluje js soubory (spojení a minifikace) a scss soubory (včetně minifikace a vendor prefixů), navýší verzi v package.json souboru


[Starší projekty](#archive) - grunt, bower atd. 
---

## CSS/SCSS
Stuktura složek a souborů, popis proměnných apod. je uvedený v souboru [readme.md](scss/readme.md) ve složce scss.

---

## Javascript
Zdrojové soubory s Javascriptem jsou umístěné ve složce _js_. Pluginy jsou umístěné ve složce _js/plugins_. Základní soubor se skripty je přímo ve složce _js_  a je pojmenovaný _main.js_. 
Soubory ke kompilaci a jejich pořadí se nastavuje v souboru _tasks/compile-js.js_, kde je proměnná (pole) `src`.
Soubory se spojí dohromady, zminifikují a uloží jako soubor _scripts.min.js_ ve složce _public_ (není-li nastaveno jinak v souboru _compile-js.js_).

---

## Ikony
SVG ikony, z kterých bude vytvořený sprite obrázek musí být umístěné ve složce _images/icon_. Po spuštění tasku `icons` se vygeneruje sprite obrázek _sprite.svg_ do složky _images_.
Ikony pak lze použít v SCSS souborech tak, že pro element zavoláme mixin `@include sprite(JMENO_SVG_SOUBORU)` (jméno svg souboru je bez přípony .svg). Elementu se nastaví zobrazení na inline-block, přiřadí se mu šířka a výška dle rozměru ikony a nastaví se pozice pozadí tak, aby byl vidět daný výřez ze sprite obrázku.
Pokud má být ikona responzivní je možné zavolat mixin ve tvaru `@include sprite(JMENO_SVG_SOUBORU,responsive)`.  Velikost pozadí je pak definována v procentech. Pro ikonu je pak ale třeba nastavit kontejner s omezenou šířkou, aby držela správný poměr stran.

---

## Starší projekty<a name="archive"></a>
U staršch projektů je kompilace CSS (SCSS nebo LESS) a JS je řešena přes [grunt](http://gruntjs.com).
Nastavení kompilace a soubory potřebné pro kompilaci jsou ve složce _.tools_.

Pro spuštění je třeba mít nainstalované:

- node.js - stáhnout a nainstalovat z https://nodejs.org/
- grunt-cli - po instalaci node stačí do příkazové řádky zadat `npm install -g grunt-cli`
 
Po instalaci výše uvedených věcí stačí ve složce _.tools_ spustit následující příkazy

    npm install
    grunt
    
Kompilace souborů s javascriptem se nastavuje v souboru _.tools/package.json_ (seznam souborů ke kompilaci a jejich pořadí) .

U některých projektů jsou závislosti pro JS a SCSS stahovány přes bower. V tomto případě je před kompilací CSS a JS ve složce _.tools_ potřeba spustit příkaz `bower install`.
Po provedení příkazu `bower install` se ve složce _.tools_ objeví složka bower_components. Tuto složky nedávejte do gitu.

---

## Často používané CSS a JS knihovny
### JS
- [Fancybox](http://fancyapps.com/fancybox/3/) - máme zakoupenou licenci
- [Slick JS slider](http://kenwheeler.github.io/slick/)
- [Air datepicker](http://t1m0n.name/air-datepicker/docs/)
- [ScrollTo](https://github.com/flesler/jquery.scrollTo)
- [Magnific Popup lightbox](http://dimsemenov.com/plugins/magnific-popup/)
- [Dropzone multiupload](http://www.dropzonejs.com)
- [Isotope masonry layout](http://isotope.metafizzy.co)
- [Flickity slider](http://flickity.metafizzy.co)
- [Easyautocomplete](http://easyautocomplete.com)
- [MatchHeight](http://brm.io/jquery-match-height/)
- [Validetta](http://lab.hasanaydogdu.com/validetta/)
- [Waypoints](http://imakewebthings.com/waypoints/)
- [FontFaceObserver](https://github.com/bramstein/fontfaceobserver)

### CSS
- [Susy grid framework](http://susy.oddbird.net)
- [SASS MQ](https://github.com/sass-mq/sass-mq)
- [Hint CSS](https://kushagragour.in/lab/hint/)
- [Breakpoint SASS](http://breakpoint-sass.com) - používáno u starších projektů pro práci s media queries
