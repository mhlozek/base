'use strict';

var SVGSpriter    = require('svg-sprite'),
path              = require('path'),
mkdirp            = require('mkdirp'),
fs                = require('fs'),
File              = require('vinyl'),
glob              = require('glob'),

config            = {
    "dest": "../images/",
    "shape": {
        "spacing": {
            "padding": 5
        }
    },
    "mode": {
        "css": {
            "dest": "",
            "sprite": "sprite.svg",
            "bust": false,
            "render": {
                "scss": {
                    "template": "../scss/nuclids/_sprite-template.scss",
                    "dest": "../scss/nuclids/_sprite.scss"
                }
            }
        }
    }
},
spriter           = new SVGSpriter(config);

var cwd = path.resolve('../images/icon');

glob.glob('**/*.svg', {cwd: cwd}, function(err, files) {
    if(err) {
        console.log(err);
    } else {
        console.log("Making icons:", files);
    }

    files.forEach(function(file){

        // Create and add a vinyl file instance for each SVG
        spriter.add(new File({
            path: path.join(cwd, file),                         // Absolute path to the SVG file
            base: cwd,                                          // Base path (see `name` argument)
            contents: fs.readFileSync(path.join(cwd, file))     // SVG file contents
        }));
    })

    spriter.compile(function(error, result, data){
        for (var type in result.css) {
            mkdirp.sync(path.dirname(result.css[type].path));
            fs.writeFileSync(result.css[type].path, result.css[type].contents);
        }
    });
});