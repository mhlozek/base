var src = [
    '../js/main.js'
];

var dest = '../public/scripts.min.js';

var fs = require('fs');
var uglify = require("uglify-js");

var uglified = uglify.minify(src);

fs.writeFile(dest, uglified.code, function (err){
    if(err) {
        console.log(err);
    } else {
        console.log("File generated and saved:", dest);
    }
});