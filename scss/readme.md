# SCSS readme

## (S)CSS
###  Struktura složek
- _atoms_ - obecné definice bez tříd nebo základní třídy použitelné kdekoli na webu
    - _base.scss
    - _base-bootstrap.scss - obecné třídy vycházející z Bootstrapu (např. zarovnání textu, skrytí prvků apod.
    - _buttons.scss	
    - _forms.scss
    - _tables.scss

- _molecules_ - každý blok, který lze použít samostatně kdekoli v layoutu
    - _nav
    - _head
    - _foot
    - _intro, _pagination apod.
		
- _nuclids_ - "programátorské věci" - proměnné a mixiny
    - _mixins
    - _mq - mixin pro zápis media queries
    - _sprite - soubor, který se generuje při konverzi svg souborů do sprite mapy
    - _sprite-mixin - mixin pro práci se svg sprite mapou
    - _sprite-template - mixin pro práci se svg sprite mapou
    - _variables - proměnné pro barvy, fonty, media queries
    - _grid - generování bootstrap gridu
    
		
- _structures_ - globální layout a layout pro konkrétní stránky
    - _layout - definice globálního layoutu

- _lib_ - typicky CSS přibalené k k jquery pluginům nebo obecné knihovny/frameworky - __zde neupravovat__. Pokud je třeba upravit výchozí vzhled, tak vytvoříme v components soubor se stejným názvem a tam předefinujeme.
    - _normalize		- sjednocení vzhledu v prohlížečích
    - _fancybox - např. css pro jquery lightbox


### Proměnné

#### Barvy
- `$color-default` - výchozí barva běžného textu
- `$color-brand` - základní "brandová" barva v grafice (např. odpovídající barvě loga apod.)
- `$color-border` - barva orámování input elementů, vodorovné linky `<hr>` apod.

#### Písmo
- `$font-main` - základní rodina písma (může být i např. Google font)
- `$font-fallback` - systémové písmo, které se použije pokud není k dispozici `$font-main` (fallback řešený přes [webfont loader](https://github.com/typekit/webfontloader) )

- `$font-size-base` - základní velikost textu
- `$font-size-large` - větší text  (např. zvýrazněná věta)
- `$font-size-small` - menší text (např. poznámky) 
- `$font-size-h1, $font-size-h2, $font-size-h3...` - definice velikostí nadpisů 

#### Breakpointy pro responzivní layout
Vychází z boostrap 4 breakpointů. U některých projektů mohou být hodnoty odlišné.

    xl: 1200px;
    lg: 992px;
    md: 768px;
    sm: 576px;
    xs: 0;
 
### Základní třídy převzaté z bootstrapu
- `text-left`, `text-right`, `text-center`, `text-justify` - zarovnání vlevo, vpravo, na střed a do bloku
- `text-nowrap` - nezalomitelný text
- `text-hide` - skryje text, použitelné i jako mixin např. při náhrazení textu obrázkem
- `text-primary` - text obarvený základní barvou `$color-brand`
- `pull-right`, `pull-left` - nastaví _float:right_ nebo _float:left_ pro element
- `invisible` - nastaví _visibility: hidden_ pro element
- `hidden` - skryje element přes _display:none_
- `show` - nastaví _display:block_ pro element